<?php
	//~ ini_set('display_errors', 1);
	//~ ini_set('display_startup_errors', 1);
	//~ error_reporting(E_ALL);
	require_once(__DIR__ . '/backend/initialize.php');
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Confirm Login</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="container">
	<div>
		<?php include(__DIR__ . '/partials/header.php'); ?>
	</div>

	<?php 
	$phone = '';
	$id = '';
	$count = 0;
	if (isset($_SESSION['otp']) && !empty($_SESSION['otp']) && $_SESSION['count']<=1 && !empty($_SESSION['otp']->phone)) {	
			$phone = $_SESSION['otp']->phone;
			$id = $_SESSION['otp']->id;
			$optnumber = mt_rand(100000, 999999);
			$url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
				'api_key' => api_key,
				'api_secret' => api_secret,
				'to' => $phone,
				'from' => api_from, 
				'text' => "One time Password ".$optnumber,
				]);
	 
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($ch);
				$response_arr = json_decode($response,true);
				$sms_info=$response_arr['messages'][0];    //print_r($sms_info);
				if($sms_info['status']==0)
				{
					$_SESSION['count']++;
					$_SESSION['newotp'] = $optnumber;
					
				}
				else
				{
				  echo "Error while sending sms ";
				  echo "SMS Status: ".$sms_info['status']."<br />";
				  echo "Error: ".$sms_info['error-text']."<br />";
				}
		
		?>
				<div>
					<h1>Confirm Login</h1>
					<form action="<?= SITEURL;?>/backend/log-user-confirm.php" method="post">
						<p>System Sent Six digit number to your registered phone number. <?php echo $phone; ?></p>
						<div class="form-group">
							<label for="otp">Enter OTP</label>
							<input type="text" id="otp" name="otp" placeholder="Enter otp" required class="form-control">
							<input type="hidden" id="otp" name="id" value="<?= $id ?>">
						</div>	
						<div>
							<input type="submit" value="Check" name="submit" class="btn btn-primary">
							<p><a href="<?php echo $_SERVER['PHP_SELF']; ?>">Retry</a></p>
						</div>
					</form>
				</div>
			</div>
			<?php
	} else { 
	$login_page = '/rateyourbook/app/login.php';
	unset($_SESSION['otp']);
	unset($_SESSION['count']);
	unset($_SESSION['newotp']);
	$_SESSION['otpmessage'] = "your otp limit is over please login agian";
	header('Location: ' .$login_page);
	}
	
?>
<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>
