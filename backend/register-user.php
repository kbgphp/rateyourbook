<?php
	// Initialize the books app
	// $app = new BooksApp();
	//~ ini_set('display_errors', 1);
	//~ ini_set('display_startup_errors', 1);
	//~ error_reporting(E_ALL);
	require_once(__DIR__ . '/initialize.php');

	$register_page = SITEURL.'/register.php';
	$login_page = SITEURL.'/login.php';
	$confirmreg = SITEURL.'/confirm-register.php';
	if (isset($_POST['submit'])) {
		// Get provided user data
		$username = $_POST['username'];
		$password = $_POST['password'];
		$date_of_birth = $_POST['dateofbirth'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$imagekey = $_POST['key'];
		// Try to rgister the user
		if ($user = $app->register_user($username, $password, $date_of_birth, $country,$state,$city, $email,$phone,$imagekey)) {
			
			if(isset($user->phone)){
				$_SESSION['otp'] = $user;
				$_SESSION['count'] = 0;
			
				//echo 	$confirmreg;
				//echo "<html><script>window.location.href=".$confirmreg."</script></html>";
				echo '<html>';
				echo '<script type="text/javascript">';
			    echo 'window.location.href="'.$confirmreg.'";';
				echo '</script>';
			    echo '<noscript>';
				echo '<meta http-equiv="refresh" content="0;url='.$confirmreg.'" />';
				echo '</noscript>';
				echo '</html>';
				 exit;			
			}else{
				$_SESSION['register'] = 'failed';
				echo '<html>';
				echo '<script type="text/javascript">';
			    echo 'window.location.href="'.$register_page.'";';
				echo '</script>';
			    echo '<noscript>';
				echo '<meta http-equiv="refresh" content="0;url='.$register_page.'" />';
				echo '</noscript>';
				echo '</html>';
				 exit;			
			}
			/*// Add message to session
			//~ $_SESSION['register'] = 'success';
			//~ header('Location: ' . $login_page);
			//~ } else {
			//~ $_SESSION['register'] = 'failed';
			//~ header('Location: ' . $register_page);
			//~ }*/
		} else {
			   echo '<html>';
				echo '<script type="text/javascript">';
			    echo 'window.location.href="'.$register_page.'";';
				echo '</script>';
			    echo '<noscript>';
				echo '<meta http-equiv="refresh" content="0;url='.$register_page.'" />';
				echo '</noscript>';
				echo '</html>';
				exit;
		}
	}
	
?>
