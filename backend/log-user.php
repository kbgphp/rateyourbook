<?php
	// Initialize the books app
	// $app = new BooksApp();
	require_once(__DIR__ . '/initialize.php');
	
	$login_page = SITEURL.'/login.php';
	$homepage = SITEURL.'/index.php';
	$confirmlogin = SITEURL.'/confirm-login.php';
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	if (isset($_POST['submit'])) {
		// Get provided username and password
		$username = $_POST['username'];
		$password = $_POST['password'];
		// Try to log in the user
		if ($user = $app->login_user($username, $password)) {			
				$_SESSION['otp'] = $user;
				$_SESSION['count'] = 0;
				header('Location: ' .$confirmlogin);
			
			// Add message to session
			
		} else {
			$_SESSION['login'] = 'failed';
			header('Location: ' . $login_page);
		}
	} else {
		header('Location: ' . $login_page);
	}
	
?>
