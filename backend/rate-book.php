<?php
	// Initialize the books app
	// $app = new BooksApp();
	require_once(__DIR__ . '/initialize.php');
	
	$single_book_page = SITEURL.'/single-book.php?isbn='.$_POST['isbn'];
	if (isset($_POST['submit']) && isset($_POST['key'])) {
		if($_SESSION['user']->imagekey == $_POST['key']){
			// Get provided user data
			$user_id = $app->user->id;
			$isbn = $_POST['isbn'];
			$comment = $_POST['comment'];
			$rating = $_POST['rating'];
			
			// Try to rate the book
			if ($app->rate_book($isbn, $user_id, $comment, $rating)) {
				// Add message to session
				$_SESSION['rate_book'] = 'success';
				//header('Location: ' . $single_book_page);
				?>
				<script type="text/javascript">window.location.href="<?=$single_book_page;?>";</script>
				<?php
			} else {
				$_SESSION['rate_book'] = 'failed';
				//header('Location: ' . $single_book_page);
				?>
				<script type="text/javascript">window.location.href="<?=$single_book_page;?>";</script>
				<?php
			}
		}else{
				$_SESSION['rate_book'] = 'key';
				//header('Location: ' . $single_book_page);
				?>
				<script type="text/javascript">window.location.href="<?=$single_book_page;?>";</script>
				<?php
		}
	} else {	?>
		<script type="text/javascript">window.location.href="<?=SITEURL.'/index.php';?>";</script>
		<?php
		//header('Location: '.SITEURL.'/index.php');
	}
	
?>
