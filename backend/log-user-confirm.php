<?php
	// Initialize the books app
	// $app = new BooksApp();
	require_once(__DIR__ . '/initialize.php');
	
	$login_page = SITEURL.'/login.php';
	$homepage = SITEURL.'/index.php';
	$confirmlogin = SITEURL.'/confirm-login.php';
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	
	if (isset($_POST['submit'])) {
		//~ // Get provided username and password
		$otp = $_POST['otp'];
		$userid = $_POST['id'];
		if($_SESSION['newotp']==$otp){
			if ($user = $app->login_user_confirm($userid)) {				
				$_SESSION['login'] = 'success';
				$_SESSION['user'] = $user;
				unset($_SESSION['otp']);
				unset($_SESSION['count']);
				unset($_SESSION['newotp']);
				header('Location: ' .$homepage);			
			} else {
				unset($_SESSION['otp']);
				unset($_SESSION['count']);
				unset($_SESSION['newotp']);
				$_SESSION['login'] = 'failed';
				header('Location: ' . $login_page);
			}
		}else{
			unset($_SESSION['newotp']);
			$_SESSION['login'] = 'failed';
			unset($_SESSION['otp']);
			unset($_SESSION['count']);
			header('Location: ' . $login_page);
		}
		// Try to log in the user
	} else {
		header('Location: ' . $login_page);
	}
	
?>
