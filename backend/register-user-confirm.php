<?php
	// Initialize the books app
	// $app = new BooksApp();
	require_once(__DIR__ . '/initialize.php');
	
	$reg_page = SITEURL.'/register.php';
	$homepage = SITEURL.'/index.php';
	$confirmreg = SITEURL.'/confirm-register.php';
	$login_page = SITEURL.'/login.php';
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	
	if (isset($_POST['submit'])) {
		//~ // Get provided username and password
		$otp = $_POST['otp'];
		$userid = $_POST['id'];
		
		if($_SESSION['newotp']==$otp){			
			if ($user = $app->register_user_confirm($userid)) {			
				$_SESSION['register'] = 'success';			   
				unset($_SESSION['otp']);
				unset($_SESSION['count']);
				unset($_SESSION['newotp']);
				 header('Location: ' . $login_page);			
			} else {
				unset($_SESSION['otp']);
				unset($_SESSION['count']);
				unset($_SESSION['newotp']);				
				$_SESSION['register'] = 'success';	
				header('Location: ' . $reg_page);
			}
		}else{
			unset($_SESSION['otp']);
			unset($_SESSION['count']);
			unset($_SESSION['newotp']);
			$_SESSION['register'] = 'failed';
			header('Location: ' . $reg_page);
		}
		// Try to log in the user
	} else {
		header('Location: ' . $reg_page);
	}
	
?>
