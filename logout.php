<?php
	session_start();
	require_once(__DIR__ . '/backend/initialize.php');
	$homepage = SITEURL.'/index.php';
	$_SESSION['logout'] = 'logout';
	if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
		unset($_SESSION['user']);
	
		echo '<html>';
		echo '<script type="text/javascript">';
		echo 'window.location.href="'.$homepage.'";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url='.$homepage.'" />';
		echo '</noscript>';
		echo '</html>';
		 exit;			
	}else{
		echo '<html>';
		echo '<script type="text/javascript">';
		echo 'window.location.href="'.$homepage.'";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url='.$homepage.'" />';
		echo '</noscript>';
		echo '</html>';
		 exit;	  
		
	}
?>
