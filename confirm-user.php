<?php
	//~ ini_set('display_errors', 1);
	//~ ini_set('display_startup_errors', 1);
	//~ error_reporting(E_ALL);
	require_once(__DIR__ . '/backend/initialize.php');

	$register_page = SITEURL.'/register.php';
	$login_page = SITEURL.'/login.php';
	$token = $_GET['token'];
	if (isset($token)) {
		if ($app->confirm_user($token)) {
			// Add message to session
			$_SESSION['register'] = 'success';
			header('Location: ' . $login_page);
		} else {
			echo "your confirmation already done.";
		}
	} else {
		header('Location: ' . $register_page);
	}
	
?>
